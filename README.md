01) 

    $(document).ready(function () {
      var countdown = $('#countdown');
      var date = {{date('m', strtotime($item->start_date))}} + '/' + {{date('d', strtotime($item->start_date))}} + '/' + {{date('Y', strtotime($item->start_date))}}
      countdown.countdown({
          date: date,
          offset: +2,
          day: 'Day',
          days: 'Days'
      });

    <ul id="countdown" class="widget-countdown">
       <li class="clock-item"><span class="count-number days"></span><p>Days</p></li>
       <li class="clock-item"><span class="count-number hours"></span><p>Hours</p></li>
       <li class="clock-item"><span class="count-number minutes"></span><p>Min</p></li>
       <li class="clock-item"><span class="count-number seconds"></span><p>Sec</p></li>
   </ul>
   
   02) jquery.countdown.min.js
   
   
   03) mullti select file javascript
   
   
   <div class="form-group">
       <label for=""><b>Details</b></label>
       <input type="file" id="details" name="details[]" class="form-control"><a href="" class="adddetails"><i class="fa fa-plus-circle fa-2x"></i></a>
       <div class="content">

       <div class="form-group"><label></label><input type="text" class="detailsrem form-control" name="details[]"><a href="" class="remvdetails"><i class="fa fa-minus-circle fa-2x"></i></a><div></div></div></div>
   </div>
                                       
  
    <script type="text/javascript">
        $(document).ready(function () {

            $(document).on('click', '.adddetails', function (e) {
               e.preventDefault();
               $('<div class="form-group"><label></label><input type="text" class="detailsrem form-control" name="details[]"><a href="" class="remvdetails"><i class="fa fa-minus-circle fa-2x"></i></a><div>').appendTo('.content');
            });
    
           $(document).on('click', '.remvdetails', function (e) {
               e.preventDefault();
               $(this).parent('div').remove();
           });
       
        });
    </script>
   
   